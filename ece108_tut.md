---
title: "All Topics Covered in Tutorial Videos"
---
# Refer to Tutorial Slides for More Information
Please refer to your tutorial slides.
Some additional examples aren't covered in videos.
* [**All Tutorial Playlist**](https://www.youtube.com/playlist?list=PLM1htQv6roG_mEkYdF63B1OADYCqoSdFz)

# Tutorial 1 PROPOSITIONAL LOGIC
* [Propositions: Operations](https://youtu.be/OmnO2f5SScc?t=33)
* [Using Set for Visualization](https://youtu.be/OmnO2f5SScc?t=333)
* [OR](https://youtu.be/OmnO2f5SScc?t=550)
* [AND](https://youtu.be/OmnO2f5SScc?t=867)
* [NOT](https://youtu.be/OmnO2f5SScc?t=1005)
* [Implication](https://youtu.be/OmnO2f5SScc?t=1091)
* [IFF](https://youtu.be/OmnO2f5SScc?t=1645)
* Contrapositive [part 1](https://youtu.be/OmnO2f5SScc?t=1900) [part 2](https://youtu.be/dwru2pF5RCk?t=1)
* [Implication involving empty sets](https://youtu.be/dwru2pF5RCk?t=375)
* [Example 1: 𝑝∨( 𝑝∧𝑞)⇔𝑝 Set approach](https://youtu.be/dwru2pF5RCk?t=700)
* [Example 1: 𝑝∨( 𝑝∧𝑞)⇔𝑝 Truth table approach](https://youtu.be/dwru2pF5RCk?t=825)
* [Example 2: rewriting ¬(𝑝⇒𝑞) in and, or, not](https://youtu.be/dwru2pF5RCk?t=1036)
* [Example 3: Transitivity shown using proof by contradiction](https://youtu.be/dwru2pF5RCk?t=1195)

# Tutorial 2 PROOF STRATEGIES
* [How to "paint" a proof](https://youtu.be/vSI7Ku9-poA?t=1)
* [Proof Techniques: Case Analysis, Direct, Contradiction](https://youtu.be/vSI7Ku9-poA?t=210)
* [Proof Techniques: Contrapositive, Construction, Induction](https://youtu.be/vSI7Ku9-poA?t=949)
* [Mathematical Induction](https://youtu.be/vSI7Ku9-poA?t=1488)
* [Steps of an Induction Proof](https://youtu.be/vSI7Ku9-poA?t=1760)
* [Important Note on Induction](https://youtu.be/vSI7Ku9-poA?t=1958)
* [How to Use Induction Efficiently](https://youtu.be/vSI7Ku9-poA?t=2099)
* [Example 1: Induction on De Morgan's](https://youtu.be/vSI7Ku9-poA?t=2225)
* [Example 2: 10^n - 1 is divisible by 3](https://youtu.be/vSI7Ku9-poA?t=2940)
* [Example 3: Sum of digit divisible by 3 implies n is divisible by 3](https://youtu.be/vSI7Ku9-poA?t=3185)

# Tutorial 3 SETS
* [Sets](https://youtu.be/GrNqE_3edaw?t=10)
* [Rule of thumb for A=B](https://youtu.be/GrNqE_3edaw?t=264)
* [Example 2: (𝐵−𝐶)∩𝐴=𝐵∩𝐴 −𝐶∩𝐴 ](https://youtu.be/GrNqE_3edaw?t=879)
* [Notation with Sets](https://youtu.be/GrNqE_3edaw?t=1743)
* Generalized Union. [part 1](https://youtu.be/GrNqE_3edaw?t=2093) [part 2](https://youtu.be/kRDXMHpcFF0?t=1)
* [Example 5: ¯((𝐴∪𝐵) )=¯𝐴∩¯𝐵](https://youtu.be/kRDXMHpcFF0?t=559)
* [Example 7: 𝐴⊆𝐵⇒𝐴∪𝐶⊆𝐵∪𝐶](https://youtu.be/kRDXMHpcFF0?t=1090)

# Tutorial 4 CARTESIAN PRODUCT & FUNCTIONS

## Cartesian Product
* [Ordered Pair & Cartesian Product](https://youtu.be/WEzA-5GLAUY?t=10)
* [Cartesian Product of 3 sets AXBXC](https://youtu.be/WEzA-5GLAUY?t=763)
* [Example 1: 𝐴×𝐵=𝐵×𝐴 implies A = B](https://youtu.be/WEzA-5GLAUY?t=872)
* [Example 2: (𝐴∩𝐵)×(𝐶∩𝐷)=(𝐴×𝐶)∩(𝐵×𝐷)](https://youtu.be/WEzA-5GLAUY?t=1125)
* Example 3: (𝐴−𝐵=𝐵)⇒(𝐴=∅) [part 1](https://youtu.be/WEzA-5GLAUY?t=1540) [part 2](https://youtu.be/4oMg8vNIUlc?t=1)


## Functions
* [Function](https://youtu.be/O3ThLRSgh58?t=1)
* [What is/is not a Function](https://youtu.be/O3ThLRSgh58?t=292)
* [Inverse Function](https://youtu.be/O3ThLRSgh58?t=543)
* [Injectivity Surjectivity Bijectivity](https://youtu.be/_DjQxzYs51Y?t=1)
* [Function Composition](https://youtu.be/_DjQxzYs51Y?t=320)
* [Example: g an f surjective, gf surjective?](https://youtu.be/_DjQxzYs51Y?t=700)
* [Example: gf bijective. g and f bijective?](https://youtu.be/_DjQxzYs51Y?t=1325)

# Tutorial 5 CARDINALITY 
* [Injective Surjective Bijective](https://youtu.be/1MKU9yymOVw?t=10)
* [Set Size related to Inj/Sur/Bij](https://youtu.be/1MKU9yymOVw?t=155)
* [Cardinality](https://youtu.be/1MKU9yymOVw?t=724)
* [Infinite sets](https://youtu.be/1MKU9yymOVw?t=1120)
* [Countable Infinite Sets](https://youtu.be/1MKU9yymOVw?t=1240)
* [Example  |Zeven| = |Z|](https://youtu.be/1MKU9yymOVw?t=1777)
* [Follow-up |Neven| = |N|](https://youtu.be/1MKU9yymOVw?t=2200)
* [Follow-up Z is Countable](https://youtu.be/1MKU9yymOVw?t=2296)
* [Example 1: 𝐴,𝐵 countable implies 𝐴∪𝐵 countable](https://youtu.be/1MKU9yymOVw?t=2918)
* [Example 2: 𝐵 count. 𝐴⊆𝐵, then 𝐴 count.](https://youtu.be/1MKU9yymOVw?t=3318)
* [Example 3: 𝐵 finite, 𝐴⊆𝐵, then 𝐴  finite](https://youtu.be/1MKU9yymOVw?t=3440)
* [Example 4: 𝐴, 𝐵 uncount is 𝐴∩𝐵 uncount?](https://youtu.be/1MKU9yymOVw?t=98)
* [Example 5:  𝐴, 𝐵 uncount is 𝐴∪𝐵 uncount?](https://youtu.be/1MKU9yymOVw?t=200)
* [Example(Hard) on AXB ](https://youtu.be/yGkBhNuDLH0?t=1)
    * [Cool Trick #1](https://youtu.be/yGkBhNuDLH0?t=110)
    * [Cool Trick #2](https://youtu.be/yGkBhNuDLH0?t=329)
    * [Construction of such Injective Function "r"](https://youtu.be/yGkBhNuDLH0?t=629)

# Tutorial 6 RELATIONS

* [Relations](https://youtu.be/9cE1ecV2kJo?t=10)
* [Relations on the same set](https://youtu.be/9cE1ecV2kJo?t=387)
* [Example 1: R=Rinverse and R asymmetric](https://youtu.be/9cE1ecV2kJo?t=1667)
* [Example 2: X is empty](https://youtu.be/9cE1ecV2kJo?t=1905)
* [Example 3: Maximum size of R](https://youtu.be/9cE1ecV2kJo?t=2030)
* [Poset, strict poset and equivalence ](https://youtu.be/tJECcc-bsac?t=1)
* [Example 4: Find the Flaw in Proof](https://youtu.be/tJECcc-bsac?t=275)
* [Equivalence Example](https://youtu.be/tJECcc-bsac?t=785)
* [Partition](https://youtu.be/O1Glo59Wqxo?t=1)
* [Equivalence Classes](https://youtu.be/O1Glo59Wqxo?t=133)
* [Example Equivalent Classes](https://youtu.be/O1Glo59Wqxo?t=473)
* [Example Related to Partition](https://youtu.be/O1Glo59Wqxo?t=1224)

# Tutorial 7 COMBINATORICS
* [How many ways of doing stuff?](https://youtu.be/x-Z6ZHGGhL8?t=20)
* [With/Without Replacement](https://youtu.be/UuD4EqTyLlw?t=1)
* [Order Matters/Doesn't Matter](https://youtu.be/UuD4EqTyLlw?t=220)
* [Example : Forming a Committee](https://youtu.be/UuD4EqTyLlw?t=435)
* [Number of Ways: order matters and with replacement](https://youtu.be/7aukXA7mgr4?t=1)
* [Number of Ways: order matters and without replacement (Permutation)](https://youtu.be/7aukXA7mgr4?t=197)
* [Number of Ways: order doesn't matter and without replacement (Combination)](https://youtu.be/7aukXA7mgr4?t=533)
* [Number of Ways: order does not matter with replacement (Multichoose)](https://youtu.be/QY6R4-HOlhY?t=1)
* [Summary of the 4 conditions](https://youtu.be/QY6R4-HOlhY?t=829)
* [Example 1: How Many Subsets?](https://youtu.be/QY6R4-HOlhY?t=863)
* [Introduction](https://youtu.be/tn6tgrVVdio?t=1)
* [Example 2: Poker Game](https://youtu.be/tn6tgrVVdio?t=16)
* [Fun Fact About Choose](https://youtu.be/tn6tgrVVdio?t=157)
* [Example 3: Phone Number](https://youtu.be/tn6tgrVVdio?t=370)
* [Example 4 part 1: Arranging "SCROOGE"](https://youtu.be/A69KarkK_v8?t=1)
* [Follow-up: Arranging "SCROOOGGGE"](https://youtu.be/A69KarkK_v8?t=335)
* [Example 4 part 2: Arranging "SCROOGE"](https://youtu.be/A69KarkK_v8?t=444)
* [Example 5: Squash Team](https://youtu.be/A69KarkK_v8?t=530)
* [Example 6: Western Romance a)](https://youtu.be/A69KarkK_v8?t=670)
* [Example 6: Western Romance b)](https://youtu.be/A69KarkK_v8?t=710)
* [Example 6: Western Romance c)](https://youtu.be/A69KarkK_v8?t=870)
* [Example 6: Western Romance d)](https://youtu.be/A69KarkK_v8?t=977)
* [Example 7: Diagonals of a Polygon](https://youtu.be/JbygAdD0JLQ?t=1)
* [Example 8: Chess Competition](https://youtu.be/JbygAdD0JLQ?t=520)
* [Sitting around Round Table](https://youtu.be/JbygAdD0JLQ?t=915)
* [Example 9: Sitting Engineers and Doctors](https://youtu.be/JbygAdD0JLQ?t=1301)

# Tutorial 8 RELATION & INCLUSION-EXCLUSION & COMBINATORICS

## More on Relation
* [Example 1: Equivalence Relationship on a Partition](https://youtu.be/_8g8i2uL3Bw) 
* [Example 2: Union and Intersection of Relations](https://youtu.be/4qOMLWVVtS4)
    * [Part a)  If 𝑅1,𝑅2 are transitive, is 𝑅1 ∩ 𝑅2 transitive?](https://youtu.be/4qOMLWVVtS4?t=1)
    * [Part b)  If 𝑅1 is reflexive, and 𝑅2 is irreflexive, is 𝑅1 ∪ 𝑅2 reflexive? irreflexive?](https://youtu.be/4qOMLWVVtS4?t=283)
    * [Part c) If 𝑅1,𝑅2 are equivalence relations, is 𝑅1 ∪ 𝑅2 an equivalence relation?](https://youtu.be/4qOMLWVVtS4?t=571)
* [Example 3: Convex Relation](https://youtu.be/j9SxDxfZtdw)

## Inclusion-Exclusion
* [A story of Double and Triple Counting](https://youtu.be/jfVmuaur4v4?t=1)
* [Remarks about the Amounts of Intersections ](https://youtu.be/jfVmuaur4v4?t=570)
* [Generalized Formula](https://youtu.be/jfVmuaur4v4?t=847)
* [Example 5 a): How many functions exists if A-&gt;B is Injective](https://youtu.be/uWEJHZguXno)
* [Example 5 a): How many functions exists if A-&gt;B is Surjective](https://youtu.be/M8G9NpoUqXQ)

## More on Combinatorics
* [Example 4: Picking Numbers In Decreasing Order](https://youtu.be/nWkj1xgMJ1U)
* [EXAMPLE 6: Placing Books](https://youtu.be/KRgVjqtBob8)
    * [a) All same colored books have to be together](https://youtu.be/KRgVjqtBob8?t=114)
    * [b) Red book at the end, and no 2 blue books can be together](https://youtu.be/KRgVjqtBob8?t=207)
    * [c) a) but Round Table](https://youtu.be/KRgVjqtBob8?t=348)

# Tutorial 9 PROBABILITY

* [Rule of Thumb 1: #Success/#Total](https://youtu.be/NvMszfoJIx8)
* [Example 1: Poker Full House](https://youtu.be/tZSyZ0couPI)
* [Rule of Thumb 2 Part 1) Independence of 2 events](https://youtu.be/vS7IHCy-SyE?t=1)
* [Rule of Thumb 2 Part 2) Probability Inclusion-Exclusion](https://youtu.be/vS7IHCy-SyE?t=253)
    * [Special Case When Events are Disjoint](https://youtu.be/vS7IHCy-SyE?t=525)
* [Example 2: Rolling a Biased Die, Pr{ODD} = 0.875](https://youtu.be/VwXjjM0ArKY?t=0)
    * [Probablity of rolling 15 odds followed by 2 evens?](https://youtu.be/VwXjjM0ArKY?t=239)
    * [Probability of rolling 15 odds and 2 evens in any order?](https://youtu.be/VwXjjM0ArKY?t=484)
* Bernoulli 
	* [part 1](https://youtu.be/RN2nMRxpj_4)
	* [part 2](https://youtu.be/Mt55lH_ETEg)
	* [part 3](https://youtu.be/s0ZHv-RTV4o)
* [Example 3: Hash Function](https://youtu.be/ujFBqSgZxcw?t=1)
* [How Many Buckets and Size of Each Bucket](https://youtu.be/ujFBqSgZxcw?t=424)
* [Jimmy's Algorithm to Find j](https://youtu.be/ujFBqSgZxcw?t=809)
* [Breaking Events into Subparts](https://youtu.be/ujFBqSgZxcw?t=904)
* [Final Formula and Solution](https://youtu.be/ujFBqSgZxcw?t=1303)

# Tutorial 9 MORE PROBABILITY 
* [Sample Space and Algebra](https://youtu.be/b3sx9MkUhXU?t=1)
* [Random Variable](https://youtu.be/b3sx9MkUhXU?t=118)
* [Expectation](https://youtu.be/b3sx9MkUhXU?t=530)
* [Coin Flip](https://youtu.be/TvfNUpeC9rA?t=1)
* [Die Roll](https://youtu.be/TvfNUpeC9rA?t=118)
* [Bernoulli](https://youtu.be/TvfNUpeC9rA?t=189)
* [\# Trials for a Success in Bernoulli](https://youtu.be/TvfNUpeC9rA?t=513)
* [Example 4: Fair Toss with Biased Coin Algorithm Description](https://youtu.be/XffhXEjme-o?t=1)
    * [Prove that Algorithm Generates a Fair Toss](https://youtu.be/XffhXEjme-o?t=153)
    * [Context on Expected Time The Algorithm will Produce a Result](https://youtu.be/XffhXEjme-o?t=294)
    * [Expected # of Coin Tosses before Generating a "Fair Toss"](https://youtu.be/XffhXEjme-o?t=558)
* [Example 5: Coin Flip Game](https://youtu.be/0Yt6LaUDQnQ?t=1)
    * [Expectation of The money We Win](https://youtu.be/0Yt6LaUDQnQ?t=120)
    * [Pr{X=0.5M}](https://youtu.be/0Yt6LaUDQnQ?t=292)
    * [Pr{X=M}](https://youtu.be/0Yt6LaUDQnQ?t=387)
    * [Pr{X=xM}](https://youtu.be/0Yt6LaUDQnQ?t=424)
    * [Algebra to solve for x](https://youtu.be/0Yt6LaUDQnQ?t=463)

# Tutorial 10 CONDITIONAL PROBABLILITY
* [Conditional Probability](https://youtu.be/qcAZPIF9JEg?t=10)
* [Bayes](https://youtu.be/qcAZPIF9JEg?t=509)
* [Independent Events](https://youtu.be/qcAZPIF9JEg?t=682)
* [Mutual Exclusive Events](https://youtu.be/qcAZPIF9JEg?t=830)
* [Example 1: Picking a coin and getting tails](https://youtu.be/iK839CVNknw?t=1)
* [Indicator Variable](https://youtu.be/445LSbSBmZA)
    * [E[Y]: Expected Number of times X=1 (A) occurs in Y = n trials](https://youtu.be/445LSbSBmZA?t=212)
    * [Crash Course on Expectation (Linearity)](https://youtu.be/445LSbSBmZA?t=343)
    * [Continuing solving for E[Y]](https://youtu.be/445LSbSBmZA?t=450)
    * [Relationship with Bernoulli](https://youtu.be/445LSbSBmZA?t=588)
* [Example 2: Given 2 coins, 1 fair 1 3/4 Head, Picking a Biased Coin if I got HH](https://youtu.be/AyKN4K98IRA?t=0)
* [Example 3: Binary Search](https://youtu.be/K-x0sKjPDxE)
    * [Indicator Function to Solve](https://youtu.be/K-x0sKjPDxE?t=254)
    * [What is Pr{Xi=1} i.e. Probability to Find j](https://youtu.be/K-x0sKjPDxE?t=477)
    * [E[Number Success in m Trials] = 1](https://youtu.be/K-x0sKjPDxE?t=659)
* [Monty Hall Problem](https://youtu.be/zGiknj_XPFQ?t=0)
    * [Bayes Strategy Pr{z|ty}](https://youtu.be/zGiknj_XPFQ?t=236)
    * [Pr{ty}](https://youtu.be/zGiknj_XPFQ?t=410)
    * [Pr{ty intersect (x complement)}](https://youtu.be/zGiknj_XPFQ?t=740)
    * [Back to Solving Pr{z|ty}](https://youtu.be/zGiknj_XPFQ?t=1030)
* [Example 4: Forming a 4-digit natural number](https://youtu.be/UYND5mvotLM?t=0)
    * [Part a) only odd digit](https://youtu.be/UYND5mvotLM?t=57)
    * [Part b) exactly 3 odd digits and no leading 0's](https://youtu.be/UYND5mvotLM?t=132)
    * [Part c) digit 7 at least once and no leading 0's](https://youtu.be/UYND5mvotLM?t=417)
    * [Part d) no equal adjacent digits and no leading 0's](https://youtu.be/UYND5mvotLM?t=684)
* [Example 5: Arranging Letters *ISOGRAM*](https://youtu.be/ugNV-ryy_6k)
    * [a) G Cannot be Immediately Followed by M](https://youtu.be/ugNV-ryy_6k?t=)
    * [c) Can't be in original position](https://youtu.be/ugNV-ryy_6k?t=134)
        * [Inclusion-Exclusion used in part c)](https://youtu.be/ugNV-ryy_6k?t=338)

